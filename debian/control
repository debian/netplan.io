Source: netplan.io
Maintainer: Debian Networking Team <team+networking@tracker.debian.org>
Uploaders:
 Andrej Shadura <andrewsh@debian.org>,
 Lukas Märdian <slyon@debian.org>,
Section: net
Priority: optional
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 pkgconf,
 bash-completion,
 libyaml-dev,
 libglib2.0-dev,
 uuid-dev,
 python3,
 python3-cffi,
 python3-coverage <!nocheck>,
 python3-pytest <!nocheck>,
 python3-pytest-cov <!nocheck>,
 python3-rich <!nocheck>,
 python3-yaml <!nocheck>,
 libcmocka-dev <!nocheck>,
 libpython3-dev,
 libsystemd-dev,
 udev <!nocheck>,
 systemd <!nocheck>,
 systemd-dev,
 dbus-x11 <!nocheck>,
 pyflakes3 <!nocheck>,
 pycodestyle <!nocheck>,
 pandoc,
 openvswitch-switch [ !armel !armhf !i386 !hppa !m68k !powerpc !sh4 !x32 ] <!nocheck>,
 meson (>= 1.3.0),
 iproute2 <!nocheck>,
Vcs-Git: https://salsa.debian.org/debian/netplan.io.git
Vcs-Browser: https://salsa.debian.org/debian/netplan.io
Homepage: https://netplan.io/

Package: netplan.io
Architecture: linux-any
Multi-Arch: foreign
Depends:
 ${python3:Depends},
 ${shlibs:Depends},
 ${misc:Depends},
 ethtool,
 iproute2,
 libnetplan1 (= ${binary:Version}),
 netplan-generator,
 python3-netplan,
 python3-yaml,
 systemd (>= 248~),
 udev,
Recommends:
 python3-rich,
Description: Declarative network configuration for various backends at runtime
 Netplan reads YAML network configuration files which are written
 by administrators, installers, cloud image instantiations, or other OS
 deployments. During early boot it then generates backend specific
 configuration files in /run to hand off control of devices to a particular
 networking daemon.
 .
 Currently supported backends are networkd and NetworkManager.
 .
 This package provides a runtime CLI to control the Netplan backends,
 e.g. to re-generate configuration or apply changes.

Package: netplan-generator
Architecture: linux-any
Multi-Arch: foreign
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libnetplan1 (= ${binary:Version}),
 systemd (>= 248~),
Suggests:
 network-manager | wpasupplicant,
 openvswitch-switch,
 iw,
Breaks: netplan.io (<< 0.106.1-8~),
Replaces: netplan.io (<< 0.106.1-8~),
Description: Declarative network configuration for various backends at boot
 Netplan reads YAML network configuration files which are written
 by administrators, installers, cloud image instantiations, or other OS
 deployments. During early boot it then generates backend specific
 configuration files in /run to hand off control of devices to a particular
 networking daemon.
 .
 Currently supported backends are networkd and NetworkManager.
 .
 This package provides a systemd-generator to configure various networking
 daemons at boot time.

Package: python3-netplan
Section: python
Architecture: linux-any
Multi-Arch: foreign
Depends:
 ${python3:Depends},
 ${shlibs:Depends},
 ${misc:Depends},
 libnetplan1 (= ${binary:Version}),
 python3-cffi-backend,
Breaks: netplan.io (<< 0.107-3~)
Replaces: netplan.io (<< 0.107-3~)
Description: Declarative network configuration Python bindings
 Netplan reads YAML network configuration files which are written
 by administrators, installers, cloud image instantiations, or other OS
 deployments. During early boot it then generates backend specific
 configuration files in /run to hand off control of devices to a particular
 networking daemon.
 .
 Currently supported backends are networkd and NetworkManager.
 .
 This package provides a CFFI based Python bindings to libnetplan.

Package: libnetplan1
Section: libs
Architecture: linux-any
Multi-Arch: same
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Description: Declarative network configuration runtime library
 Netplan reads YAML network configuration files which are written
 by administrators, installers, cloud image instantiations, or other OS
 deployments. During early boot it then generates backend specific
 configuration files in /run to hand off control of devices to a particular
 networking daemon.
 .
 Currently supported backends are networkd and NetworkManager.
 .
 This package contains the necessary runtime library files.

Package: libnetplan-dev
Section: libdevel
Architecture: linux-any
Multi-Arch: same
Depends: ${misc:Depends},
 libnetplan1 (= ${binary:Version}),
Description: Development files for Netplan's libnetplan runtime library
 Netplan reads YAML network configuration files which are written
 by administrators, installers, cloud image instantiations, or other OS
 deployments. During early boot it then generates backend specific
 configuration files in /run to hand off control of devices to a particular
 networking daemon.
 .
 Currently supported backends are networkd and NetworkManager.
 .
 This package contains development files for developers wanting to use
 libnetplan in their applications.
